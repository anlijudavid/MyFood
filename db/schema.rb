# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150607235117) do

  create_table "AUD_personas", id: false, force: :cascade do |t|
    t.string   "Nombre1",                  limit: 255
    t.string   "Nombre2",                  limit: 255
    t.string   "Apellido1",                limit: 255
    t.string   "Apellido2",                limit: 255
    t.date     "FechNac"
    t.string   "NumTelefonoPersonal",      limit: 255
    t.string   "NumTelefonoTrabajo",       limit: 255
    t.string   "Email1",                   limit: 255
    t.string   "Email2",                   limit: 255
    t.string   "TipoIdentificacion",       limit: 255
    t.string   "NumeroIdentificacion",     limit: 255
    t.string   "Genero",                   limit: 255
    t.integer  "Estado",                   limit: 4
    t.string   "Direccion",                limit: 255
    t.integer  "Departamento",             limit: 4
    t.integer  "Imagen_id",                limit: 4
    t.datetime "created_at",                           null: false
    t.string   "Nombre1_OLD",              limit: 255
    t.string   "Nombre2_OLD",              limit: 255
    t.string   "Apellido1_OLD",            limit: 255
    t.string   "Apellido2_OLD",            limit: 255
    t.date     "FechNac_OLD"
    t.string   "NumTelefonoPersonal_OLD",  limit: 255
    t.string   "NumTelefonoTrabajo_OLD",   limit: 255
    t.string   "Email1_OLD",               limit: 255
    t.string   "Email2_OLD",               limit: 255
    t.string   "TipoIdentificacion_OLD",   limit: 255
    t.string   "NumeroIdentificacion_OLD", limit: 255
    t.string   "Genero_OLD",               limit: 255
    t.integer  "Estado_OLD",               limit: 4
    t.string   "Direccion_OLD",            limit: 255
    t.integer  "Departamento_OLD",         limit: 4
    t.integer  "Imagen_id_OLD",            limit: 4
    t.date     "fecha_actualizacion"
    t.string   "usuario",                  limit: 400
  end

  create_table "clientes", force: :cascade do |t|
    t.integer  "Estado",     limit: 4
    t.integer  "Persona_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "clientes", ["Persona_id"], name: "fk_rails_4a33f82d19", using: :btree

  create_table "departamentos", force: :cascade do |t|
    t.string   "NombreDepartamento", limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "detalle_menu_dia", force: :cascade do |t|
    t.text     "Nota",        limit: 65535
    t.integer  "Plato_id",    limit: 4
    t.integer  "Menu_Dia_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "detalle_menu_dia", ["Menu_Dia_id"], name: "fk_rails_9c0e275c42", using: :btree
  add_index "detalle_menu_dia", ["Plato_id"], name: "fk_rails_62279e78d5", using: :btree

  create_table "detalle_pedidos", force: :cascade do |t|
    t.integer  "Cantidad",   limit: 4
    t.decimal  "IVA",                  precision: 10
    t.integer  "Plato_id",   limit: 4
    t.integer  "Pedido_id",  limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "detalle_pedidos", ["Pedido_id"], name: "fk_rails_d31de940fc", using: :btree
  add_index "detalle_pedidos", ["Plato_id"], name: "fk_rails_8656bd21e1", using: :btree

  create_table "detalle_platos", force: :cascade do |t|
    t.integer  "CantidadIngrediente", limit: 4
    t.integer  "Plato_id",            limit: 4
    t.integer  "Ingrediente_id",      limit: 4
    t.integer  "Plato_componente_id", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "detalle_platos", ["Ingrediente_id"], name: "fk_rails_c2e7d816db", using: :btree
  add_index "detalle_platos", ["Plato_componente_id"], name: "fk_rails_eff6637ee4", using: :btree
  add_index "detalle_platos", ["Plato_id"], name: "fk_rails_5c381e0a1f", using: :btree

  create_table "detalle_sumistros", force: :cascade do |t|
    t.decimal  "Cantidad",                   precision: 10
    t.integer  "PrecioUnitario",   limit: 4
    t.integer  "IVA",              limit: 4
    t.decimal  "Subtotal",                   precision: 10
    t.date     "FechaVencimiento"
    t.integer  "Ingrediente_id",   limit: 4
    t.integer  "Suministro_id",    limit: 4
    t.integer  "UnidadMedida_id",  limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "detalle_sumistros", ["Ingrediente_id"], name: "fk_rails_635d9d2bb6", using: :btree
  add_index "detalle_sumistros", ["Suministro_id"], name: "fk_rails_364be6d0bd", using: :btree
  add_index "detalle_sumistros", ["UnidadMedida_id"], name: "fk_rails_4ad2dd4b49", using: :btree

  create_table "detalle_valor_nutricionals", force: :cascade do |t|
    t.decimal  "Cantidad",                         precision: 10
    t.string   "Porcentaje",           limit: 255
    t.integer  "UnidadMedida_id",      limit: 4
    t.integer  "NombreNutricional_id", limit: 4
    t.integer  "ValorNutricional_id",  limit: 4
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "detalle_valor_nutricionals", ["NombreNutricional_id"], name: "fk_rails_6d54e7b5cb", using: :btree
  add_index "detalle_valor_nutricionals", ["UnidadMedida_id"], name: "fk_rails_09e91445bb", using: :btree
  add_index "detalle_valor_nutricionals", ["ValorNutricional_id"], name: "fk_rails_092900f254", using: :btree

  create_table "empleados", force: :cascade do |t|
    t.binary   "Estado",     limit: 65535
    t.integer  "Rol_id",     limit: 4
    t.integer  "Persona_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "empleados", ["Persona_id"], name: "fk_rails_88f3acca37", using: :btree
  add_index "empleados", ["Rol_id"], name: "fk_rails_52298d6b48", using: :btree

  create_table "empresas", force: :cascade do |t|
    t.string   "Nombre_empresa", limit: 255
    t.string   "NIT",            limit: 255
    t.string   "Direccion",      limit: 255
    t.string   "Ciudad",         limit: 255
    t.string   "Telefono",       limit: 255
    t.string   "Celular",        limit: 255
    t.string   "Correo1",        limit: 255
    t.string   "Correo2",        limit: 255
    t.string   "PaginaWeb",      limit: 255
    t.binary   "Estado",         limit: 65535
    t.binary   "Proveedor",      limit: 65535
    t.integer  "Imagen_logo_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "empresas", ["Imagen_logo_id"], name: "fk_rails_bb40f84df6", using: :btree

  create_table "estado_pedidos", force: :cascade do |t|
    t.string   "NombreEstado", limit: 255
    t.text     "Detalles",     limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "imagens", force: :cascade do |t|
    t.text     "NombreImagen",       limit: 65535
    t.integer  "Estado",             limit: 4
    t.string   "Descripcion",        limit: 500
    t.binary   "Contenido",          limit: 65535
    t.integer  "ancho",              limit: 4
    t.integer  "alto",               limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
  end

  create_table "ingredientes", force: :cascade do |t|
    t.string   "NombreIngrediente",  limit: 255
    t.text     "Detalles",           limit: 65535
    t.integer  "IVA",                limit: 4
    t.integer  "TipoIngrediente_id", limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "ingredientes", ["TipoIngrediente_id"], name: "fk_rails_4390724963", using: :btree

  create_table "menu_dia", force: :cascade do |t|
    t.string   "Dia",        limit: 255
    t.binary   "Estado",     limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "mesas", force: :cascade do |t|
    t.integer  "NumeroMesa",      limit: 4
    t.binary   "Disponibilidad",  limit: 65535
    t.text     "Nota",            limit: 65535
    t.integer  "Maximo_personas", limit: 4
    t.string   "Estado",          limit: 255
    t.string   "Detalle",         limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "municipios", force: :cascade do |t|
    t.string   "NombreMunicipio", limit: 255
    t.integer  "departamento_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "municipios", ["departamento_id"], name: "fk_rails_3e2b31ed2a", using: :btree

  create_table "nombre_nutricionals", force: :cascade do |t|
    t.string   "NombresNutricional", limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "pedidos", force: :cascade do |t|
    t.datetime "FechaPedido"
    t.string   "Estado",          limit: 50
    t.float    "total_pagar",     limit: 24
    t.float    "subtotal",        limit: 24
    t.float    "iva_total",       limit: 24
    t.integer  "Mesa_id",         limit: 4
    t.integer  "Empleado_id",     limit: 4
    t.integer  "EstadoPedido_id", limit: 4
    t.integer  "Cliente_id",      limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "pedidos", ["Cliente_id"], name: "fk_rails_84a8b1b8c9", using: :btree
  add_index "pedidos", ["Empleado_id"], name: "fk_rails_95f79364d3", using: :btree
  add_index "pedidos", ["EstadoPedido_id"], name: "fk_rails_39ff71bb27", using: :btree
  add_index "pedidos", ["Mesa_id"], name: "fk_rails_cad4ba84f5", using: :btree

  create_table "permisos", force: :cascade do |t|
    t.string   "NombrePermiso", limit: 255
    t.boolean  "Eliminar",      limit: 1
    t.boolean  "Modificar",     limit: 1
    t.boolean  "Ver",           limit: 1
    t.integer  "rols_id",       limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "permisos", ["rols_id"], name: "fk_rails_9c08aeec56", using: :btree

  create_table "personas", force: :cascade do |t|
    t.string   "Nombre1",              limit: 255
    t.string   "Nombre2",              limit: 255
    t.string   "Apellido1",            limit: 255
    t.string   "Apellido2",            limit: 255
    t.date     "FechNac"
    t.string   "NumTelefonoPersonal",  limit: 255
    t.string   "NumTelefonoTrabajo",   limit: 255
    t.string   "Email1",               limit: 255
    t.string   "Email2",               limit: 255
    t.string   "TipoIdentificacion",   limit: 255
    t.string   "NumeroIdentificacion", limit: 255
    t.string   "Genero",               limit: 255
    t.integer  "Estado",               limit: 4
    t.string   "Direccion",            limit: 255
    t.integer  "Departamento_id",      limit: 4
    t.integer  "Imagen_id",            limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "personas", ["Departamento_id"], name: "fk_rails_b587fd6fb8", using: :btree
  add_index "personas", ["Imagen_id"], name: "fk_rails_a2ad4165e1", using: :btree

  create_table "platos", force: :cascade do |t|
    t.string   "NombrePlato",         limit: 255
    t.decimal  "PrecioUnitario",                  precision: 10
    t.integer  "Minutos_preparacion", limit: 4
    t.integer  "Estado",              limit: 4
    t.string   "Descripcion",         limit: 255
    t.integer  "tipo_plato_id",       limit: 4
    t.integer  "imagen_id",           limit: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  add_index "platos", ["imagen_id"], name: "fk_rails_f4fc58b190", using: :btree
  add_index "platos", ["tipo_plato_id"], name: "fk_rails_7034378d44", using: :btree

  create_table "rols", force: :cascade do |t|
    t.string   "NombreRol",  limit: 255
    t.text     "Nota",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "suministros", force: :cascade do |t|
    t.datetime "FechaSumnistro"
    t.decimal  "TotalPagar",                 precision: 10
    t.string   "EstadoPago",     limit: 255
    t.integer  "Empresa_id",     limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "suministros", ["Empresa_id"], name: "fk_rails_20c8908e28", using: :btree

  create_table "tipo_ingredientes", force: :cascade do |t|
    t.string   "NombreTipoIngrediente", limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "tipo_platos", force: :cascade do |t|
    t.string   "NombreTipoPlato", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "unidad_medidas", force: :cascade do |t|
    t.string   "Unidad",         limit: 255
    t.string   "NombreCompleto", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "Empleado_id",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "usuarios", ["Empleado_id"], name: "fk_rails_4fadfec06f", using: :btree
  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true, using: :btree
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true, using: :btree

  create_table "valor_nutricionals", force: :cascade do |t|
    t.decimal  "CantidadXPorcion",             precision: 10
    t.string   "Descripcion",      limit: 255
    t.integer  "UnidadMedida_id",  limit: 4
    t.integer  "Ingrediente_id",   limit: 4
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "valor_nutricionals", ["Ingrediente_id"], name: "fk_rails_fe7b671b37", using: :btree
  add_index "valor_nutricionals", ["UnidadMedida_id"], name: "fk_rails_93df46e12d", using: :btree

  create_table "view_reporte_pedidos_all", id: false, force: :cascade do |t|
    t.integer  "id",              limit: 4,                    default: 0, null: false
    t.datetime "FechaPedido"
    t.string   "Estado",          limit: 50
    t.float    "total_pagar",     limit: 24
    t.float    "subtotal",        limit: 24
    t.float    "iva_total",       limit: 24
    t.integer  "NumeroMesa",      limit: 4
    t.text     "Empleado",        limit: 65535
    t.string   "NombreEstado",    limit: 255
    t.text     "Cliente",         limit: 65535
    t.string   "NombrePlato",     limit: 255
    t.decimal  "PrecioUnitario",                precision: 10
    t.integer  "Cantidad",        limit: 4
    t.string   "NombreTipoPlato", limit: 255
  end

  add_foreign_key "clientes", "personas", column: "Persona_id"
  add_foreign_key "detalle_menu_dia", "menu_dia", column: "Menu_Dia_id"
  add_foreign_key "detalle_menu_dia", "platos", column: "Plato_id"
  add_foreign_key "detalle_pedidos", "pedidos", column: "Pedido_id"
  add_foreign_key "detalle_pedidos", "platos", column: "Plato_id"
  add_foreign_key "detalle_platos", "ingredientes", column: "Ingrediente_id"
  add_foreign_key "detalle_platos", "platos", column: "Plato_componente_id"
  add_foreign_key "detalle_platos", "platos", column: "Plato_id"
  add_foreign_key "detalle_sumistros", "ingredientes", column: "Ingrediente_id"
  add_foreign_key "detalle_sumistros", "suministros", column: "Suministro_id"
  add_foreign_key "detalle_sumistros", "unidad_medidas", column: "UnidadMedida_id"
  add_foreign_key "detalle_valor_nutricionals", "nombre_nutricionals", column: "NombreNutricional_id"
  add_foreign_key "detalle_valor_nutricionals", "unidad_medidas", column: "UnidadMedida_id"
  add_foreign_key "detalle_valor_nutricionals", "valor_nutricionals", column: "ValorNutricional_id"
  add_foreign_key "empleados", "personas", column: "Persona_id"
  add_foreign_key "empleados", "rols", column: "Rol_id"
  add_foreign_key "empresas", "imagens", column: "Imagen_logo_id"
  add_foreign_key "ingredientes", "tipo_ingredientes", column: "TipoIngrediente_id"
  add_foreign_key "municipios", "departamentos"
  add_foreign_key "pedidos", "clientes", column: "Cliente_id"
  add_foreign_key "pedidos", "empleados", column: "Empleado_id"
  add_foreign_key "pedidos", "estado_pedidos", column: "EstadoPedido_id"
  add_foreign_key "pedidos", "mesas", column: "Mesa_id"
  add_foreign_key "permisos", "rols", column: "rols_id"
  add_foreign_key "personas", "departamentos", column: "Departamento_id"
  add_foreign_key "personas", "imagens", column: "Imagen_id"
  add_foreign_key "platos", "imagens"
  add_foreign_key "platos", "tipo_platos"
  add_foreign_key "suministros", "empresas", column: "Empresa_id"
  add_foreign_key "usuarios", "empleados", column: "Empleado_id"
  add_foreign_key "valor_nutricionals", "ingredientes", column: "Ingrediente_id"
  add_foreign_key "valor_nutricionals", "unidad_medidas", column: "UnidadMedida_id"
end
