class CreateSuministros < ActiveRecord::Migration
  def change
    create_table :suministros do |t|
      t.datetime :FechaSumnistro
      t.decimal :TotalPagar
      t.string :EstadoPago
      t.integer :Empresa_id

      t.timestamps null: false
    end

    add_foreign_key :suministros, :empresas, column: :Empresa_id, primary_key: "id"
  end
end
