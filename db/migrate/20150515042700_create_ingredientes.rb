class CreateIngredientes < ActiveRecord::Migration
  def change
    create_table :ingredientes do |t|
      t.string :NombreIngrediente
      t.text :Detalles
      t.integer :IVA
      t.integer :TipoIngrediente_id

      t.timestamps null: false
    end

    add_foreign_key :ingredientes, :tipo_ingredientes, column: :TipoIngrediente_id, primary_key: "id"
  end
end
