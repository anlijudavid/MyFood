class CreateTipoPlatos < ActiveRecord::Migration
  def change
    create_table :tipo_platos do |t|
      t.string :NombreTipoPlato

      t.timestamps null: false
    end
  end
end
