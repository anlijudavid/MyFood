class CreateDetallePedidos < ActiveRecord::Migration
  def change
    create_table :detalle_pedidos do |t|
      t.integer :Cantidad
      t.decimal :Subtotal
      t.decimal :IVA
      t.decimal :totalpagar
      t.integer :Plato_id
      t.integer :Pedido_id

      t.timestamps null: false
    end

    add_foreign_key :detalle_pedidos, :platos, column: :Plato_id, primary_key: "id"
    add_foreign_key :detalle_pedidos, :pedidos, column: :Pedido_id, primary_key: "id"
  end
end
