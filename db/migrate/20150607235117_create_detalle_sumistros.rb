class CreateDetalleSumistros < ActiveRecord::Migration
  def change
    create_table :detalle_sumistros do |t|
      t.decimal :Cantidad
      t.integer :PrecioUnitario
      t.integer :IVA
      t.decimal :Subtotal
      t.date :FechaVencimiento
      t.integer :Ingrediente_id
      t.integer :Suministro_id
      t.integer :UnidadMedida_id

      t.timestamps null: false
    end

    add_foreign_key :detalle_sumistros, :ingredientes, column: :Ingrediente_id, primary_key: "id"
    add_foreign_key :detalle_sumistros, :suministros, column: :Suministro_id, primary_key: "id"
    add_foreign_key :detalle_sumistros, :unidad_medidas, column: :UnidadMedida_id, primary_key: "id"
  end
end
