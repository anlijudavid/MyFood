class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :Nombre_empresa
      t.string :NIT
      t.string :Direccion
      t.string :Ciudad
      t.string :Telefono
      t.string :Celular
      t.string :Correo1
      t.string :Correo2
      t.string :PaginaWeb
      t.binary :Estado
      t.binary :Proveedor
      t.integer :Imagen_logo_id

      t.timestamps null: false
    end

    add_foreign_key :empresas, :imagens, column: :Imagen_logo_id, primary_key: "id"
  end
end
