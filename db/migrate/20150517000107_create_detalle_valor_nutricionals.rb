class CreateDetalleValorNutricionals < ActiveRecord::Migration
  def change
    create_table :detalle_valor_nutricionals do |t|
      t.decimal :Cantidad
      t.string :Porcentaje
      t.integer :UnidadMedida_id
      t.integer :NombreNutricional_id
      t.integer :ValorNutricional_id

      t.timestamps null: false
    end

    add_foreign_key :detalle_valor_nutricionals, :unidad_medidas, column: :UnidadMedida_id, primary_key: "id"
    add_foreign_key :detalle_valor_nutricionals, :nombre_nutricionals, column: :NombreNutricional_id, primary_key: "id"
    add_foreign_key :detalle_valor_nutricionals, :valor_nutricionals, column: :ValorNutricional_id, primary_key: "id"
  end
end