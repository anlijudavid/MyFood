class CreateClientes < ActiveRecord::Migration
  def change
    create_table :clientes do |t|
      t.integer :Estado
      t.integer :Persona_id

      t.timestamps null: false
    end

    add_foreign_key :clientes, :personas, column: :Persona_id, primary_key: "id"
  end
end
