class CreateMenuDia < ActiveRecord::Migration
  def change
    create_table :menu_dia do |t|
      t.string :Dia
      t.binary :Estado

      t.timestamps null: false
    end
  end
end
