class CreateValorNutricionals < ActiveRecord::Migration
  def change
    create_table :valor_nutricionals do |t|
      t.decimal :CantidadXPorcion
      t.string :Descripcion
      t.integer :UnidadMedida_id
      t.integer :Ingrediente_id

      t.timestamps null: false
    end

    add_foreign_key :valor_nutricionals, :unidad_medidas, column: :UnidadMedida_id, primary_key: "id"
    add_foreign_key :valor_nutricionals, :ingredientes, column: :Ingrediente_id, primary_key: "id"
  end
end
