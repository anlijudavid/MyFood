class CreateEmpleados < ActiveRecord::Migration
  def change
    create_table :empleados do |t|
      t.binary :Estado
      t.integer :Rol_id
      t.integer :Persona_id

      t.timestamps null: false
    end

    add_foreign_key :empleados, :rols, column: :Rol_id, primary_key: "id"
    add_foreign_key :empleados, :personas, column: :Persona_id, primary_key: "id"
  end
end
