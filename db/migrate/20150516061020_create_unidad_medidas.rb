class CreateUnidadMedidas < ActiveRecord::Migration
  def change
    create_table :unidad_medidas do |t|
      t.string :Unidad
      t.string :NombreCompleto

      t.timestamps null: false
    end
  end
end
