class CreatePermisos < ActiveRecord::Migration
  def change
    create_table :permisos do |t|
      t.string :NombrePermiso
      t.boolean :Eliminar
      t.boolean :Modificar
      t.boolean :Ver
      t.integer :rols_id

      t.timestamps null: false
    end

    add_foreign_key :permisos, :rols, column: :rols_id, primary_key: "id"
  end
end
