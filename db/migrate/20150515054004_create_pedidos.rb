class CreatePedidos < ActiveRecord::Migration
  def change
    create_table :pedidos do |t|
      t.datetime :FechaPedido
      t.binary :Estado
      t.integer :total_pagar
      t.integer :subtotal
      t.integer :Mesa_id
      t.integer :Empleado_id
      t.integer :EstadoPedido_id
      t.integer :Cliente_id

      t.timestamps null: false
    end

    add_foreign_key :pedidos, :mesas, column: :Mesa_id, primary_key: "id"
    add_foreign_key :pedidos, :empleados, column: :Empleado_id, primary_key: "id"
    add_foreign_key :pedidos, :estado_pedidos, column: :EstadoPedido_id, primary_key: "id"
    add_foreign_key :pedidos, :clientes, column: :Cliente_id, primary_key: "id"
  end
end
