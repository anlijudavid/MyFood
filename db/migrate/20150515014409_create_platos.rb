class CreatePlatos < ActiveRecord::Migration
  def change
    create_table :platos do |t|
      t.string :NombrePlato
      t.decimal :PrecioUnitario
      t.integer :Minutos_preparacion
      t.integer :Estado
      t.string :Descripcion
      t.integer :tipo_plato_id
      t.integer :imagen_id

      t.timestamps null: false
    end

#   add_foreign_key :articles, :users, column: :author_id, primary_key: "lng_id"
    #ALTER TABLE "articles" ADD CONSTRAINT fk_rails_58ca3d3a82 FOREIGN KEY ("author_id") REFERENCES "users" ("lng_id")
    add_foreign_key :platos, :tipo_platos, column: :tipo_plato_id, primary_key: "id"
    add_foreign_key :platos, :imagens, column: :imagen_id, primary_key: "id"
  end
end
