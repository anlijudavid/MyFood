class CreateRols < ActiveRecord::Migration
  def change
    create_table :rols do |t|
      t.string :NombreRol
      t.text :Nota

      t.timestamps null: false
    end
  end
end
