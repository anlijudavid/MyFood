class CreateMesas < ActiveRecord::Migration
  def change
    create_table :mesas do |t|
      t.integer :NumeroMesa
      t.binary :Disponibilidad
      t.text :Nota
      t.integer :Maximo_personas
      t.string :Estado
      t.string :Detalle

      t.timestamps null: false
    end
  end
end
