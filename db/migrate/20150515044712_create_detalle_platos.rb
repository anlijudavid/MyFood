class CreateDetallePlatos < ActiveRecord::Migration
  def change
    create_table :detalle_platos do |t|
      t.integer :CantidadIngrediente
      t.integer :Plato_id
      t.integer :Ingrediente_id
      t.integer :Plato_componente_id

      t.timestamps null: false
    end

    add_foreign_key :detalle_platos, :platos, column: :Plato_id, primary_key: "id"
    add_foreign_key :detalle_platos, :ingredientes, column: :Ingrediente_id, primary_key: "id"
    add_foreign_key :detalle_platos, :platos, column: :Plato_componente_id, primary_key: "id"
  end
end
