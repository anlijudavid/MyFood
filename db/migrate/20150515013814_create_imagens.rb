class CreateImagens < ActiveRecord::Migration
  def change
    create_table :imagens do |t|
      t.text    :NombreImagen
      t.integer :Estado
      t.string  :Descripcion,  limit: 500

      t.timestamps null: false
    end

    add_attachment :imagens, :image
  end
end
