class CreatePersonas < ActiveRecord::Migration
  def change
    create_table :personas do |t|
      t.string :Nombre1
      t.string :Nombre2
      t.string :Apellido1
      t.string :Apellido2
      t.date :FechNac
      t.string :NumTelefonoPersonal
      t.string :NumTelefonoTrabajo
      t.string :Email1
      t.string :Email2
      t.string :TipoIdentificacion
      t.string :NumeroIdentificacion
      t.string :Genero
      t.integer :Estado
      t.string :Direccion
      t.integer :Departamento_id
      t.integer :Imagen_id

      t.timestamps null: false
    end

    add_foreign_key :personas, :departamentos, column: :Departamento_id, primary_key: "id"
    add_foreign_key :personas, :imagens, column: :Imagen_id, primary_key: "id"
  end
end
