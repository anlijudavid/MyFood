class CreateTipoIngredientes < ActiveRecord::Migration
  def change
    create_table :tipo_ingredientes do |t|
      t.string :NombreTipoIngrediente

      t.timestamps null: false
    end
  end
end
