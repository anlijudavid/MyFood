class CreateDetalleMenuDia < ActiveRecord::Migration
  def change
    create_table :detalle_menu_dia do |t|
      t.text :Nota
      t.integer :Plato_id
      t.integer :Menu_Dia_id

      t.timestamps null: false
    end

    add_foreign_key :detalle_menu_dia, :platos, column: :Plato_id, primary_key: "id"
    add_foreign_key :detalle_menu_dia, :menu_dia, column: :Menu_Dia_id, primary_key: "id"
  end
end
