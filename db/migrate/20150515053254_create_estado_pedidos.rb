class CreateEstadoPedidos < ActiveRecord::Migration
  def change
    create_table :estado_pedidos do |t|
      t.string :NombreEstado
      t.text :Detalles

      t.timestamps null: false
    end
  end
end
