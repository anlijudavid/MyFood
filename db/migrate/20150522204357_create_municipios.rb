class CreateMunicipios < ActiveRecord::Migration
  def change
    create_table :municipios do |t|
      t.string :NombreMunicipio
      t.integer :departamento_id

      t.timestamps null: false
    end

    add_foreign_key :municipios, :departamentos, column: :departamento_id, primary_key: "id"
  end
end
