require 'test_helper'

class NombreNutricionalsControllerTest < ActionController::TestCase
  setup do
    @nombre_nutricional = nombre_nutricionals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:nombre_nutricionals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create nombre_nutricional" do
    assert_difference('NombreNutricional.count') do
      post :create, nombre_nutricional: { NombresNutricional: @nombre_nutricional.NombresNutricional }
    end

    assert_redirected_to nombre_nutricional_path(assigns(:nombre_nutricional))
  end

  test "should show nombre_nutricional" do
    get :show, id: @nombre_nutricional
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @nombre_nutricional
    assert_response :success
  end

  test "should update nombre_nutricional" do
    patch :update, id: @nombre_nutricional, nombre_nutricional: { NombresNutricional: @nombre_nutricional.NombresNutricional }
    assert_redirected_to nombre_nutricional_path(assigns(:nombre_nutricional))
  end

  test "should destroy nombre_nutricional" do
    assert_difference('NombreNutricional.count', -1) do
      delete :destroy, id: @nombre_nutricional
    end

    assert_redirected_to nombre_nutricionals_path
  end
end
