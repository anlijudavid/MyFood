require 'test_helper'

class PersonasControllerTest < ActionController::TestCase
  setup do
    @persona = personas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:personas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create persona" do
    assert_difference('Persona.count') do
      post :create, persona: { Apellido1: @persona.Apellido1, Apellido2: @persona.Apellido2, Departamento_id: @persona.Departamento_id, Direccion: @persona.Direccion, Email1: @persona.Email1, Email2: @persona.Email2, Estado: @persona.Estado, FechNac: @persona.FechNac, Genero: @persona.Genero, Imagen_id: @persona.Imagen_id, Nombre1: @persona.Nombre1, Nombre2: @persona.Nombre2, NumTelefonoPersonal: @persona.NumTelefonoPersonal, NumTelefonoTrabajo: @persona.NumTelefonoTrabajo, NumeroIdentificacion: @persona.NumeroIdentificacion, TipoIdentificacion: @persona.TipoIdentificacion }
    end

    assert_redirected_to persona_path(assigns(:persona))
  end

  test "should show persona" do
    get :show, id: @persona
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @persona
    assert_response :success
  end

  test "should update persona" do
    patch :update, id: @persona, persona: { Apellido1: @persona.Apellido1, Apellido2: @persona.Apellido2, Departamento_id: @persona.Departamento_id, Direccion: @persona.Direccion, Email1: @persona.Email1, Email2: @persona.Email2, Estado: @persona.Estado, FechNac: @persona.FechNac, Genero: @persona.Genero, Imagen_id: @persona.Imagen_id, Nombre1: @persona.Nombre1, Nombre2: @persona.Nombre2, NumTelefonoPersonal: @persona.NumTelefonoPersonal, NumTelefonoTrabajo: @persona.NumTelefonoTrabajo, NumeroIdentificacion: @persona.NumeroIdentificacion, TipoIdentificacion: @persona.TipoIdentificacion }
    assert_redirected_to persona_path(assigns(:persona))
  end

  test "should destroy persona" do
    assert_difference('Persona.count', -1) do
      delete :destroy, id: @persona
    end

    assert_redirected_to personas_path
  end
end
