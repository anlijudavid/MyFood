require 'test_helper'

class DetallePlatosControllerTest < ActionController::TestCase
  setup do
    @detalle_plato = detalle_platos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:detalle_platos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create detalle_plato" do
    assert_difference('DetallePlato.count') do
      post :create, detalle_plato: { CantidadIngrediente: @detalle_plato.CantidadIngrediente, Ingrediente_id: @detalle_plato.Ingrediente_id, Plato_componente_id: @detalle_plato.Plato_componente_id, Plato_id: @detalle_plato.Plato_id }
    end

    assert_redirected_to detalle_plato_path(assigns(:detalle_plato))
  end

  test "should show detalle_plato" do
    get :show, id: @detalle_plato
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @detalle_plato
    assert_response :success
  end

  test "should update detalle_plato" do
    patch :update, id: @detalle_plato, detalle_plato: { CantidadIngrediente: @detalle_plato.CantidadIngrediente, Ingrediente_id: @detalle_plato.Ingrediente_id, Plato_componente_id: @detalle_plato.Plato_componente_id, Plato_id: @detalle_plato.Plato_id }
    assert_redirected_to detalle_plato_path(assigns(:detalle_plato))
  end

  test "should destroy detalle_plato" do
    assert_difference('DetallePlato.count', -1) do
      delete :destroy, id: @detalle_plato
    end

    assert_redirected_to detalle_platos_path
  end
end
