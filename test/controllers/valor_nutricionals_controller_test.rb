require 'test_helper'

class ValorNutricionalsControllerTest < ActionController::TestCase
  setup do
    @valor_nutricional = valor_nutricionals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:valor_nutricionals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create valor_nutricional" do
    assert_difference('ValorNutricional.count') do
      post :create, valor_nutricional: { CantidadXPorcion: @valor_nutricional.CantidadXPorcion, Descripcion: @valor_nutricional.Descripcion, Ingrediente_id: @valor_nutricional.Ingrediente_id, UnidadMedida_id: @valor_nutricional.UnidadMedida_id }
    end

    assert_redirected_to valor_nutricional_path(assigns(:valor_nutricional))
  end

  test "should show valor_nutricional" do
    get :show, id: @valor_nutricional
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @valor_nutricional
    assert_response :success
  end

  test "should update valor_nutricional" do
    patch :update, id: @valor_nutricional, valor_nutricional: { CantidadXPorcion: @valor_nutricional.CantidadXPorcion, Descripcion: @valor_nutricional.Descripcion, Ingrediente_id: @valor_nutricional.Ingrediente_id, UnidadMedida_id: @valor_nutricional.UnidadMedida_id }
    assert_redirected_to valor_nutricional_path(assigns(:valor_nutricional))
  end

  test "should destroy valor_nutricional" do
    assert_difference('ValorNutricional.count', -1) do
      delete :destroy, id: @valor_nutricional
    end

    assert_redirected_to valor_nutricionals_path
  end
end
