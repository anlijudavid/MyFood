require 'test_helper'

class TipoIngredientesControllerTest < ActionController::TestCase
  setup do
    @tipo_ingrediente = tipo_ingredientes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipo_ingredientes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipo_ingrediente" do
    assert_difference('TipoIngrediente.count') do
      post :create, tipo_ingrediente: { NombreTipoIngrediente: @tipo_ingrediente.NombreTipoIngrediente }
    end

    assert_redirected_to tipo_ingrediente_path(assigns(:tipo_ingrediente))
  end

  test "should show tipo_ingrediente" do
    get :show, id: @tipo_ingrediente
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipo_ingrediente
    assert_response :success
  end

  test "should update tipo_ingrediente" do
    patch :update, id: @tipo_ingrediente, tipo_ingrediente: { NombreTipoIngrediente: @tipo_ingrediente.NombreTipoIngrediente }
    assert_redirected_to tipo_ingrediente_path(assigns(:tipo_ingrediente))
  end

  test "should destroy tipo_ingrediente" do
    assert_difference('TipoIngrediente.count', -1) do
      delete :destroy, id: @tipo_ingrediente
    end

    assert_redirected_to tipo_ingredientes_path
  end
end
