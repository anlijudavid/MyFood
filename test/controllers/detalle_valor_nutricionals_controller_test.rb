require 'test_helper'

class DetalleValorNutricionalsControllerTest < ActionController::TestCase
  setup do
    @detalle_valor_nutricional = detalle_valor_nutricionals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:detalle_valor_nutricionals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create detalle_valor_nutricional" do
    assert_difference('DetalleValorNutricional.count') do
      post :create, detalle_valor_nutricional: { Cantidad: @detalle_valor_nutricional.Cantidad, NombreNutricional_id: @detalle_valor_nutricional.NombreNutricional_id, Porcentaje: @detalle_valor_nutricional.Porcentaje, UnidadMedida_id: @detalle_valor_nutricional.UnidadMedida_id, ValorNutricional_id: @detalle_valor_nutricional.ValorNutricional_id }
    end

    assert_redirected_to detalle_valor_nutricional_path(assigns(:detalle_valor_nutricional))
  end

  test "should show detalle_valor_nutricional" do
    get :show, id: @detalle_valor_nutricional
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @detalle_valor_nutricional
    assert_response :success
  end

  test "should update detalle_valor_nutricional" do
    patch :update, id: @detalle_valor_nutricional, detalle_valor_nutricional: { Cantidad: @detalle_valor_nutricional.Cantidad, NombreNutricional_id: @detalle_valor_nutricional.NombreNutricional_id, Porcentaje: @detalle_valor_nutricional.Porcentaje, UnidadMedida_id: @detalle_valor_nutricional.UnidadMedida_id, ValorNutricional_id: @detalle_valor_nutricional.ValorNutricional_id }
    assert_redirected_to detalle_valor_nutricional_path(assigns(:detalle_valor_nutricional))
  end

  test "should destroy detalle_valor_nutricional" do
    assert_difference('DetalleValorNutricional.count', -1) do
      delete :destroy, id: @detalle_valor_nutricional
    end

    assert_redirected_to detalle_valor_nutricionals_path
  end
end
