require 'test_helper'

class EmpresasControllerTest < ActionController::TestCase
  setup do
    @empresa = empresas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:empresas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create empresa" do
    assert_difference('Empresa.count') do
      post :create, empresa: { Celular: @empresa.Celular, Ciudad: @empresa.Ciudad, Correo1: @empresa.Correo1, Correo2: @empresa.Correo2, Direccion: @empresa.Direccion, Estado: @empresa.Estado, Imagen_logo_id: @empresa.Imagen_logo_id, NIT: @empresa.NIT, Nombre_empresa: @empresa.Nombre_empresa, PaginaWeb: @empresa.PaginaWeb, Proveedor: @empresa.Proveedor, Telefono: @empresa.Telefono }
    end

    assert_redirected_to empresa_path(assigns(:empresa))
  end

  test "should show empresa" do
    get :show, id: @empresa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @empresa
    assert_response :success
  end

  test "should update empresa" do
    patch :update, id: @empresa, empresa: { Celular: @empresa.Celular, Ciudad: @empresa.Ciudad, Correo1: @empresa.Correo1, Correo2: @empresa.Correo2, Direccion: @empresa.Direccion, Estado: @empresa.Estado, Imagen_logo_id: @empresa.Imagen_logo_id, NIT: @empresa.NIT, Nombre_empresa: @empresa.Nombre_empresa, PaginaWeb: @empresa.PaginaWeb, Proveedor: @empresa.Proveedor, Telefono: @empresa.Telefono }
    assert_redirected_to empresa_path(assigns(:empresa))
  end

  test "should destroy empresa" do
    assert_difference('Empresa.count', -1) do
      delete :destroy, id: @empresa
    end

    assert_redirected_to empresas_path
  end
end
