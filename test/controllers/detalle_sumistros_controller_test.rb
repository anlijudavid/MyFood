require 'test_helper'

class DetalleSumistrosControllerTest < ActionController::TestCase
  setup do
    @detalle_sumistro = detalle_sumistros(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:detalle_sumistros)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create detalle_sumistro" do
    assert_difference('DetalleSumistro.count') do
      post :create, detalle_sumistro: { Cantidad: @detalle_sumistro.Cantidad, FechaVencimiento: @detalle_sumistro.FechaVencimiento, IVA: @detalle_sumistro.IVA, Ingrediente_id: @detalle_sumistro.Ingrediente_id, PrecioUnitario: @detalle_sumistro.PrecioUnitario, Subtotal: @detalle_sumistro.Subtotal, Suministro_id: @detalle_sumistro.Suministro_id, UnidadMedida_id: @detalle_sumistro.UnidadMedida_id }
    end

    assert_redirected_to detalle_sumistro_path(assigns(:detalle_sumistro))
  end

  test "should show detalle_sumistro" do
    get :show, id: @detalle_sumistro
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @detalle_sumistro
    assert_response :success
  end

  test "should update detalle_sumistro" do
    patch :update, id: @detalle_sumistro, detalle_sumistro: { Cantidad: @detalle_sumistro.Cantidad, FechaVencimiento: @detalle_sumistro.FechaVencimiento, IVA: @detalle_sumistro.IVA, Ingrediente_id: @detalle_sumistro.Ingrediente_id, PrecioUnitario: @detalle_sumistro.PrecioUnitario, Subtotal: @detalle_sumistro.Subtotal, Suministro_id: @detalle_sumistro.Suministro_id, UnidadMedida_id: @detalle_sumistro.UnidadMedida_id }
    assert_redirected_to detalle_sumistro_path(assigns(:detalle_sumistro))
  end

  test "should destroy detalle_sumistro" do
    assert_difference('DetalleSumistro.count', -1) do
      delete :destroy, id: @detalle_sumistro
    end

    assert_redirected_to detalle_sumistros_path
  end
end
