require 'test_helper'

class DetalleMenuDiaControllerTest < ActionController::TestCase
  setup do
    @detalle_menu_dium = detalle_menu_dia(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:detalle_menu_dia)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create detalle_menu_dium" do
    assert_difference('DetalleMenuDium.count') do
      post :create, detalle_menu_dium: { Menu_Dia_id: @detalle_menu_dium.Menu_Dia_id, Nota: @detalle_menu_dium.Nota, Plato_id: @detalle_menu_dium.Plato_id }
    end

    assert_redirected_to detalle_menu_dium_path(assigns(:detalle_menu_dium))
  end

  test "should show detalle_menu_dium" do
    get :show, id: @detalle_menu_dium
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @detalle_menu_dium
    assert_response :success
  end

  test "should update detalle_menu_dium" do
    patch :update, id: @detalle_menu_dium, detalle_menu_dium: { Menu_Dia_id: @detalle_menu_dium.Menu_Dia_id, Nota: @detalle_menu_dium.Nota, Plato_id: @detalle_menu_dium.Plato_id }
    assert_redirected_to detalle_menu_dium_path(assigns(:detalle_menu_dium))
  end

  test "should destroy detalle_menu_dium" do
    assert_difference('DetalleMenuDium.count', -1) do
      delete :destroy, id: @detalle_menu_dium
    end

    assert_redirected_to detalle_menu_dia_path
  end
end
