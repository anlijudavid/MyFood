require 'test_helper'

class MenuDiaControllerTest < ActionController::TestCase
  setup do
    @menu_dium = menu_dia(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:menu_dia)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create menu_dium" do
    assert_difference('MenuDium.count') do
      post :create, menu_dium: { Dia: @menu_dium.Dia, Estado: @menu_dium.Estado }
    end

    assert_redirected_to menu_dium_path(assigns(:menu_dium))
  end

  test "should show menu_dium" do
    get :show, id: @menu_dium
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @menu_dium
    assert_response :success
  end

  test "should update menu_dium" do
    patch :update, id: @menu_dium, menu_dium: { Dia: @menu_dium.Dia, Estado: @menu_dium.Estado }
    assert_redirected_to menu_dium_path(assigns(:menu_dium))
  end

  test "should destroy menu_dium" do
    assert_difference('MenuDium.count', -1) do
      delete :destroy, id: @menu_dium
    end

    assert_redirected_to menu_dia_path
  end
end
