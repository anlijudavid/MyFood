module ApplicationHelper

  def verificarPermiso(form)
    sql = "select per.id from usuarios us inner join empleados em on em.id = us.Empleado_id inner join rols r on r.id = em.Rol_id inner join permisos per on per.rols_id = r.id where per.NombrePermiso LIKE '#{form}' && email = '#{ current_usuario.email }'"
    id_rol = Permiso.find_by_sql sql
    if id_rol.nil?
      raise "No se encontro ROL"
    else
      per = Permiso.find_by_rols_id id_rol
      if per.nil?
        raise "No se encontraron permisos, verifique la siguiente consulta: #{sql}"
      end
      @@ver_p = per.Ver
      @@modificar_p = per.Modificar
      @@editar_p = per.Modificar
      @@eliminar_p = per.Eliminar
    end
  end

end
