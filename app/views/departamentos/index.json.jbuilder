json.array!(@departamentos) do |departamento|
  json.extract! departamento, :id, :NombreDepartamento
  json.url departamento_url(departamento, format: :json)
end
