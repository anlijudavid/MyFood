json.array!(@detalle_pedidos) do |detalle_pedido|
  json.extract! detalle_pedido, :id, :Cantidad, :Subtotal, :IVA, :totalpagar, :Plato_id, :Pedido_id
  json.url detalle_pedido_url(detalle_pedido, format: :json)
end
