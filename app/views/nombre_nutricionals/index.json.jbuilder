json.array!(@nombre_nutricionals) do |nombre_nutricional|
  json.extract! nombre_nutricional, :id, :NombresNutricional
  json.url nombre_nutricional_url(nombre_nutricional, format: :json)
end
