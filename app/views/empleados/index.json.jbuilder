json.array!(@empleados) do |empleado|
  json.extract! empleado, :id, :Estado, :Rol_id, :Persona_id
  json.url empleado_url(empleado, format: :json)
end
