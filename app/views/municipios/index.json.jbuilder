json.array!(@municipios) do |municipio|
  json.extract! municipio, :id, :NombreMunicipio, :departamento_id
  json.url municipio_url(municipio, format: :json)
end
