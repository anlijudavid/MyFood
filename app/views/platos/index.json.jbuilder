json.array!(@platos) do |plato|
  json.extract! plato, :id, :NombrePlato, :PrecioUnitario, :Minutos_preparacion, :Estado, :Descripcion, :TipoPlato_id, :Imagen_id
  json.url plato_url(plato, format: :json)
end
