json.array!(@valor_nutricionals) do |valor_nutricional|
  json.extract! valor_nutricional, :id, :CantidadXPorcion, :Descripcion, :UnidadMedida_id, :Ingrediente_id
  json.url valor_nutricional_url(valor_nutricional, format: :json)
end
