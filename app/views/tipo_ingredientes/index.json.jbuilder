json.array!(@tipo_ingredientes) do |tipo_ingrediente|
  json.extract! tipo_ingrediente, :id, :NombreTipoIngrediente
  json.url tipo_ingrediente_url(tipo_ingrediente, format: :json)
end
