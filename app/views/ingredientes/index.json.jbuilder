json.array!(@ingredientes) do |ingrediente|
  json.extract! ingrediente, :id, :NombreIngrediente, :Detalles, :IVA, :TipoIngrediente_id
  json.url ingrediente_url(ingrediente, format: :json)
end
