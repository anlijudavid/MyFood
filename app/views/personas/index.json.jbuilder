json.array!(@personas) do |persona|
  json.extract! persona, :id, :Nombre1, :Nombre2, :Apellido1, :Apellido2, :FechNac, :NumTelefonoPersonal, :NumTelefonoTrabajo, :Email1, :Email2, :TipoIdentificacion, :NumeroIdentificacion, :Genero, :Estado, :Direccion, :Departamento_id, :Imagen_id
  json.url persona_url(persona, format: :json)
end
