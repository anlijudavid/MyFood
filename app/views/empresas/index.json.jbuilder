json.array!(@empresas) do |empresa|
  json.extract! empresa, :id, :Nombre_empresa, :NIT, :Direccion, :Ciudad, :Telefono, :Celular, :Correo1, :Correo2, :PaginaWeb, :Estado, :Proveedor, :Imagen_logo_id
  json.url empresa_url(empresa, format: :json)
end
