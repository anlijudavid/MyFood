json.array!(@mesas) do |mesa|
  json.extract! mesa, :id, :NumeroMesa, :Disponibilidad, :Nota, :Maximo_personas, :Estado, :Detalle
  json.url mesa_url(mesa, format: :json)
end
