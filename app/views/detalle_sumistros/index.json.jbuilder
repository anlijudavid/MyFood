json.array!(@detalle_sumistros) do |detalle_sumistro|
  json.extract! detalle_sumistro, :id, :Cantidad, :PrecioUnitario, :IVA, :Subtotal, :FechaVencimiento, :Ingrediente_id, :Suministro_id, :UnidadMedida_id
  json.url detalle_sumistro_url(detalle_sumistro, format: :json)
end
