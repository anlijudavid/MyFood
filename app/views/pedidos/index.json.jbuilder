json.array!(@pedidos) do |pedido|
  json.extract! pedido, :id, :FechaPedido, :Estado, :Mesa_id, :Empleado_id, :EstadoPedido_id, :Cliente_id
  json.url pedido_url(pedido, format: :json)
end
