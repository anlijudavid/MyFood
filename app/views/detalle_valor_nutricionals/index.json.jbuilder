json.array!(@detalle_valor_nutricionals) do |detalle_valor_nutricional|
  json.extract! detalle_valor_nutricional, :id, :Cantidad, :Porcentaje, :UnidadMedida_id, :NombreNutricional_id, :ValorNutricional_id
  json.url detalle_valor_nutricional_url(detalle_valor_nutricional, format: :json)
end
