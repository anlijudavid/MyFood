class NombreNutricionalsController < ApplicationController
  before_action :set_nombre_nutricional, only: [:show, :edit, :update, :destroy]

  # GET /nombre_nutricionals
  # GET /nombre_nutricionals.json
  def index
    @nombre_nutricionals = NombreNutricional.all
  end

  # GET /nombre_nutricionals/1
  # GET /nombre_nutricionals/1.json
  def show
  end

  # GET /nombre_nutricionals/new
  def new
    @nombre_nutricional = NombreNutricional.new
  end

  # GET /nombre_nutricionals/1/edit
  def edit
  end

  # POST /nombre_nutricionals
  # POST /nombre_nutricionals.json
  def create
    @nombre_nutricional = NombreNutricional.new(nombre_nutricional_params)

    respond_to do |format|
      if @nombre_nutricional.save
        format.html { redirect_to @nombre_nutricional, notice: 'Nombre nutricional was successfully created.' }
        format.json { render :show, status: :created, location: @nombre_nutricional }
      else
        format.html { render :new }
        format.json { render json: @nombre_nutricional.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nombre_nutricionals/1
  # PATCH/PUT /nombre_nutricionals/1.json
  def update
    respond_to do |format|
      if @nombre_nutricional.update(nombre_nutricional_params)
        format.html { redirect_to @nombre_nutricional, notice: 'Nombre nutricional was successfully updated.' }
        format.json { render :show, status: :ok, location: @nombre_nutricional }
      else
        format.html { render :edit }
        format.json { render json: @nombre_nutricional.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nombre_nutricionals/1
  # DELETE /nombre_nutricionals/1.json
  def destroy
    @nombre_nutricional.destroy
    respond_to do |format|
      format.html { redirect_to nombre_nutricionals_url, notice: 'Nombre nutricional was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nombre_nutricional
      @nombre_nutricional = NombreNutricional.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nombre_nutricional_params
      params.require(:nombre_nutricional).permit(:NombresNutricional)
    end
end
