class DetallePlatosController < ApplicationController
  before_action :set_detalle_plato, only: [:show, :edit, :update, :destroy]

  def ValidarUsuario
    if !usuario_signed_in?
      redirect_to new_usuario_session_path
    end
  end

  # GET /detalle_platos
  # GET /detalle_platos.json
  def index
    @detalle_platos = DetallePlato.all
  end

  # GET /detalle_platos/1
  # GET /detalle_platos/1.json
  def show
  end

  # GET /detalle_platos/new
  def new
    @detalle_plato = DetallePlato.new
  end

  # GET /detalle_platos/1/edit
  def edit
  end

  # POST /detalle_platos
  # POST /detalle_platos.json
  def create
    @detalle_plato = DetallePlato.new(detalle_plato_params)

    respond_to do |format|
      if @detalle_plato.save
        format.html { redirect_to @detalle_plato, notice: 'Detalle plato was successfully created.' }
        format.json { render :show, status: :created, location: @detalle_plato }
      else
        format.html { render :new }
        format.json { render json: @detalle_plato.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /detalle_platos/1
  # PATCH/PUT /detalle_platos/1.json
  def update
    respond_to do |format|
      if @detalle_plato.update(detalle_plato_params)
        format.html { redirect_to @detalle_plato, notice: 'Detalle plato was successfully updated.' }
        format.json { render :show, status: :ok, location: @detalle_plato }
      else
        format.html { render :edit }
        format.json { render json: @detalle_plato.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /detalle_platos/1
  # DELETE /detalle_platos/1.json
  def destroy
    @detalle_plato.destroy
    respond_to do |format|
      format.html { redirect_to detalle_platos_url, notice: 'Detalle plato was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detalle_plato
      @detalle_plato = DetallePlato.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def detalle_plato_params
      params.require(:detalle_plato).permit(:CantidadIngrediente, :Plato_id, :Ingrediente_id, :Plato_componente_id)
    end
end
