class DetalleMenuDiaController < ApplicationController
  before_action :set_detalle_menu_dium, only: [:show, :edit, :update, :destroy]

  def ValidarUsuario
    if !usuario_signed_in?
      redirect_to new_usuario_session_path
    end
  end

  # GET /detalle_menu_dia
  # GET /detalle_menu_dia.json
  def index
    ValidarUsuario()
    @detalle_menu_dia = DetalleMenuDium.all
  end

  # GET /detalle_menu_dia/1
  # GET /detalle_menu_dia/1.json
  def show
    ValidarUsuario()
  end

  # GET /detalle_menu_dia/new
  def new
    ValidarUsuario()
    @detalle_menu_dium = DetalleMenuDium.new
  end

  # GET /detalle_menu_dia/1/edit
  def edit
    ValidarUsuario()
  end

  # POST /detalle_menu_dia
  # POST /detalle_menu_dia.json
  def create
    ValidarUsuario()
    @detalle_menu_dium = DetalleMenuDium.new(detalle_menu_dium_params)

    respond_to do |format|
      if @detalle_menu_dium.save
        format.html { redirect_to @detalle_menu_dium, notice: 'Detalle menu dium was successfully created.' }
        format.json { render :show, status: :created, location: @detalle_menu_dium }
      else
        format.html { render :new }
        format.json { render json: @detalle_menu_dium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /detalle_menu_dia/1
  # PATCH/PUT /detalle_menu_dia/1.json
  def update
    ValidarUsuario()
    respond_to do |format|
      if @detalle_menu_dium.update(detalle_menu_dium_params)
        format.html { redirect_to @detalle_menu_dium, notice: 'Detalle menu dium was successfully updated.' }
        format.json { render :show, status: :ok, location: @detalle_menu_dium }
      else
        format.html { render :edit }
        format.json { render json: @detalle_menu_dium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /detalle_menu_dia/1
  # DELETE /detalle_menu_dia/1.json
  def destroy
    ValidarUsuario()
    @detalle_menu_dium.destroy
    respond_to do |format|
      format.html { redirect_to detalle_menu_dia_url, notice: 'Detalle menu dium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detalle_menu_dium
      @detalle_menu_dium = DetalleMenuDium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def detalle_menu_dium_params
      params.require(:detalle_menu_dium).permit(:Nota, :Plato_id, :Menu_Dia_id)
    end
end
