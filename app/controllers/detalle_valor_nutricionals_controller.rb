class DetalleValorNutricionalsController < ApplicationController
  before_action :set_detalle_valor_nutricional, only: [:show, :edit, :update, :destroy]

  # GET /detalle_valor_nutricionals
  # GET /detalle_valor_nutricionals.json
  def index
    @detalle_valor_nutricionals = DetalleValorNutricional.all
  end

  # GET /detalle_valor_nutricionals/1
  # GET /detalle_valor_nutricionals/1.json
  def show
  end

  # GET /detalle_valor_nutricionals/new
  def new
    @detalle_valor_nutricional = DetalleValorNutricional.new
  end

  # GET /detalle_valor_nutricionals/1/edit
  def edit
  end

  # POST /detalle_valor_nutricionals
  # POST /detalle_valor_nutricionals.json
  def create
    @detalle_valor_nutricional = DetalleValorNutricional.new(detalle_valor_nutricional_params)

    respond_to do |format|
      if @detalle_valor_nutricional.save
        format.html { redirect_to @detalle_valor_nutricional, notice: 'Detalle valor nutricional was successfully created.' }
        format.json { render :show, status: :created, location: @detalle_valor_nutricional }
      else
        format.html { render :new }
        format.json { render json: @detalle_valor_nutricional.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /detalle_valor_nutricionals/1
  # PATCH/PUT /detalle_valor_nutricionals/1.json
  def update
    respond_to do |format|
      if @detalle_valor_nutricional.update(detalle_valor_nutricional_params)
        format.html { redirect_to @detalle_valor_nutricional, notice: 'Detalle valor nutricional was successfully updated.' }
        format.json { render :show, status: :ok, location: @detalle_valor_nutricional }
      else
        format.html { render :edit }
        format.json { render json: @detalle_valor_nutricional.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /detalle_valor_nutricionals/1
  # DELETE /detalle_valor_nutricionals/1.json
  def destroy
    @detalle_valor_nutricional.destroy
    respond_to do |format|
      format.html { redirect_to detalle_valor_nutricionals_url, notice: 'Detalle valor nutricional was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detalle_valor_nutricional
      @detalle_valor_nutricional = DetalleValorNutricional.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def detalle_valor_nutricional_params
      params.require(:detalle_valor_nutricional).permit(:Cantidad, :Porcentaje, :UnidadMedida_id, :NombreNutricional_id, :ValorNutricional_id)
    end
end
