class ValorNutricionalsController < ApplicationController
  before_action :set_valor_nutricional, only: [:show, :edit, :update, :destroy]

  # GET /valor_nutricionals
  # GET /valor_nutricionals.json
  def index
    @valor_nutricionals = ValorNutricional.all
  end

  # GET /valor_nutricionals/1
  # GET /valor_nutricionals/1.json
  def show
  end

  # GET /valor_nutricionals/new
  def new
    @valor_nutricional = ValorNutricional.new
  end

  # GET /valor_nutricionals/1/edit
  def edit
  end

  # POST /valor_nutricionals
  # POST /valor_nutricionals.json
  def create
    @valor_nutricional = ValorNutricional.new(valor_nutricional_params)

    respond_to do |format|
      if @valor_nutricional.save
        format.html { redirect_to @valor_nutricional, notice: 'Valor nutricional was successfully created.' }
        format.json { render :show, status: :created, location: @valor_nutricional }
      else
        format.html { render :new }
        format.json { render json: @valor_nutricional.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /valor_nutricionals/1
  # PATCH/PUT /valor_nutricionals/1.json
  def update
    respond_to do |format|
      if @valor_nutricional.update(valor_nutricional_params)
        format.html { redirect_to @valor_nutricional, notice: 'Valor nutricional was successfully updated.' }
        format.json { render :show, status: :ok, location: @valor_nutricional }
      else
        format.html { render :edit }
        format.json { render json: @valor_nutricional.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /valor_nutricionals/1
  # DELETE /valor_nutricionals/1.json
  def destroy
    @valor_nutricional.destroy
    respond_to do |format|
      format.html { redirect_to valor_nutricionals_url, notice: 'Valor nutricional was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_valor_nutricional
      @valor_nutricional = ValorNutricional.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def valor_nutricional_params
      params.require(:valor_nutricional).permit(:CantidadXPorcion, :Descripcion, :UnidadMedida_id, :Ingrediente_id)
    end
end
