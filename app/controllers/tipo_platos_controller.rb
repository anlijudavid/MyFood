class TipoPlatosController < ApplicationController
  before_action :set_tipo_plato, only: [:show, :edit, :update, :destroy]

  # GET /tipo_platos
  # GET /tipo_platos.json
  def index
    @tipo_platos = TipoPlato.all
  end

  # GET /tipo_platos/1
  # GET /tipo_platos/1.json
  def show
  end

  # GET /tipo_platos/new
  def new
    @tipo_plato = TipoPlato.new
  end

  # GET /tipo_platos/1/edit
  def edit
  end

  # POST /tipo_platos
  # POST /tipo_platos.json
  def create
    @tipo_plato = TipoPlato.new(tipo_plato_params)

    respond_to do |format|
      if @tipo_plato.save
        format.html { redirect_to @tipo_plato, notice: 'Tipo plato was successfully created.' }
        format.json { render :show, status: :created, location: @tipo_plato }
      else
        format.html { render :new }
        format.json { render json: @tipo_plato.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tipo_platos/1
  # PATCH/PUT /tipo_platos/1.json
  def update
    respond_to do |format|
      if @tipo_plato.update(tipo_plato_params)
        format.html { redirect_to @tipo_plato, notice: 'Tipo plato was successfully updated.' }
        format.json { render :show, status: :ok, location: @tipo_plato }
      else
        format.html { render :edit }
        format.json { render json: @tipo_plato.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipo_platos/1
  # DELETE /tipo_platos/1.json
  def destroy
    @tipo_plato.destroy
    respond_to do |format|
      format.html { redirect_to tipo_platos_url, notice: 'Tipo plato was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_plato
      @tipo_plato = TipoPlato.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tipo_plato_params
      params.require(:tipo_plato).permit(:NombreTipoPlato)
    end
end
