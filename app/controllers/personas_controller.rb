class PersonasController < ApplicationController
  before_action :set_persona, only: [:show, :edit, :update, :destroy]
  include ApplicationHelper

  # GET /personas
  # GET /personas.json
  def index
    #  verificarPermiso "Personas"
    #  if !@@ver_p
    #    redirect_to root_path
    #  end
    @personas = Persona.all
  end

  # GET /personas/1
  # GET /personas/1.json
  def show
    #  verificarPermiso "Personas"
    #  if !@@ver_p
      redirect_to root_path
    # end
  end

  # GET /personas/new
  def new
    #  verificarPermiso "Personas"
    # if !@@modificar_p
    #    redirect_to root_path
      #  end

    @persona = Persona.new
  end

  # GET /personas/1/edit
  def edit
    #   if !@@modificar_p
    #    redirect_to root_path
    #  end
  end

  # POST /personas
  # POST /personas.json
  def create
    #  if !@@modificar_p
    #    redirect_to root_path
    #  end

    @persona = Persona.new(persona_params)

    respond_to do |format|
      if @persona.save
        format.html { redirect_to @persona, notice: 'Persona was successfully created.' }
        format.json { render :show, status: :created, location: @persona }
      else
        format.html { render :new }
        format.json { render json: @persona.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /personas/1
  # PATCH/PUT /personas/1.json
  def update
    #   if !@@modificar_p
      #     redirect_to root_path
    #  end

    respond_to do |format|
      if @persona.update(persona_params)
        format.html { redirect_to @persona, notice: 'Persona was successfully updated.' }
        format.json { render :show, status: :ok, location: @persona }
      else
        format.html { render :edit }
        format.json { render json: @persona.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /personas/1
  # DELETE /personas/1.json
  def destroy
    #  verificarPermiso "Personas"
    #  if !@@eliminar_p
    #    redirect_to root_path
    #  else
      @persona.destroy
      respond_to do |format|
        format.html { redirect_to personas_url, notice: 'Persona was successfully destroyed.' }
        format.json { head :no_content }
      end
    #  end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_persona
      @persona = Persona.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def persona_params
      params.require(:persona).permit(:Nombre1, :Nombre2, :Apellido1, :Apellido2, :FechNac, :NumTelefonoPersonal, :NumTelefonoTrabajo, :Email1, :Email2, :TipoIdentificacion, :NumeroIdentificacion, :Genero, :Estado, :Direccion, :Departamento_id, :Imagen_id)
    end
end
