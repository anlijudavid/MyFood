class DetalleSumistrosController < ApplicationController
  before_action :set_detalle_sumistro, only: [:show, :edit, :update, :destroy]

  # GET /detalle_sumistros
  # GET /detalle_sumistros.json
  def index
    @detalle_sumistros = DetalleSumistro.all
  end

  # GET /detalle_sumistros/1
  # GET /detalle_sumistros/1.json
  def show
  end

  # GET /detalle_sumistros/new
  def new
    @detalle_sumistro = DetalleSumistro.new
  end

  # GET /detalle_sumistros/1/edit
  def edit
  end

  # POST /detalle_sumistros
  # POST /detalle_sumistros.json
  def create
    @detalle_sumistro = DetalleSumistro.new(detalle_sumistro_params)

    respond_to do |format|
      if @detalle_sumistro.save
        format.html { redirect_to @detalle_sumistro, notice: 'Detalle sumistro was successfully created.' }
        format.json { render :show, status: :created, location: @detalle_sumistro }
      else
        format.html { render :new }
        format.json { render json: @detalle_sumistro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /detalle_sumistros/1
  # PATCH/PUT /detalle_sumistros/1.json
  def update
    respond_to do |format|
      if @detalle_sumistro.update(detalle_sumistro_params)
        format.html { redirect_to @detalle_sumistro, notice: 'Detalle sumistro was successfully updated.' }
        format.json { render :show, status: :ok, location: @detalle_sumistro }
      else
        format.html { render :edit }
        format.json { render json: @detalle_sumistro.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /detalle_sumistros/1
  # DELETE /detalle_sumistros/1.json
  def destroy
    @detalle_sumistro.destroy
    respond_to do |format|
      format.html { redirect_to detalle_sumistros_url, notice: 'Detalle sumistro was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detalle_sumistro
      @detalle_sumistro = DetalleSumistro.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def detalle_sumistro_params
      params.require(:detalle_sumistro).permit(:Cantidad, :PrecioUnitario, :IVA, :Subtotal, :FechaVencimiento, :Ingrediente_id, :Suministro_id, :UnidadMedida_id)
    end
end
