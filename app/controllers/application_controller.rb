class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  # before_filter :authenticate_user!

  # config.action_controller.include_all_helpers = false

end
