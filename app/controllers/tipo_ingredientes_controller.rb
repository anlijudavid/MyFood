class TipoIngredientesController < ApplicationController
  before_action :set_tipo_ingrediente, only: [:show, :edit, :update, :destroy]

  # GET /tipo_ingredientes
  # GET /tipo_ingredientes.json
  def index
    @tipo_ingredientes = TipoIngrediente.all
  end

  # GET /tipo_ingredientes/1
  # GET /tipo_ingredientes/1.json
  def show
  end

  # GET /tipo_ingredientes/new
  def new
    @tipo_ingrediente = TipoIngrediente.new
  end

  # GET /tipo_ingredientes/1/edit
  def edit
  end

  # POST /tipo_ingredientes
  # POST /tipo_ingredientes.json
  def create
    @tipo_ingrediente = TipoIngrediente.new(tipo_ingrediente_params)

    respond_to do |format|
      if @tipo_ingrediente.save
        format.html { redirect_to @tipo_ingrediente, notice: 'Tipo ingrediente was successfully created.' }
        format.json { render :show, status: :created, location: @tipo_ingrediente }
      else
        format.html { render :new }
        format.json { render json: @tipo_ingrediente.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tipo_ingredientes/1
  # PATCH/PUT /tipo_ingredientes/1.json
  def update
    respond_to do |format|
      if @tipo_ingrediente.update(tipo_ingrediente_params)
        format.html { redirect_to @tipo_ingrediente, notice: 'Tipo ingrediente was successfully updated.' }
        format.json { render :show, status: :ok, location: @tipo_ingrediente }
      else
        format.html { render :edit }
        format.json { render json: @tipo_ingrediente.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipo_ingredientes/1
  # DELETE /tipo_ingredientes/1.json
  def destroy
    @tipo_ingrediente.destroy
    respond_to do |format|
      format.html { redirect_to tipo_ingredientes_url, notice: 'Tipo ingrediente was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_ingrediente
      @tipo_ingrediente = TipoIngrediente.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tipo_ingrediente_params
      params.require(:tipo_ingrediente).permit(:NombreTipoIngrediente)
    end
end
