class MenuDiaController < ApplicationController
  before_action :set_menu_dium, only: [:show, :edit, :update, :destroy]

  # GET /menu_dia
  # GET /menu_dia.json
  def index
    @menu_dia = MenuDium.all
  end

  # GET /menu_dia/1
  # GET /menu_dia/1.json
  def show
  end

  # GET /menu_dia/new
  def new
    @menu_dium = MenuDium.new
  end

  # GET /menu_dia/1/edit
  def edit
  end

  # POST /menu_dia
  # POST /menu_dia.json
  def create
    @menu_dium = MenuDium.new(menu_dium_params)

    respond_to do |format|
      if @menu_dium.save
        format.html { redirect_to @menu_dium, notice: 'Menu dium was successfully created.' }
        format.json { render :show, status: :created, location: @menu_dium }
      else
        format.html { render :new }
        format.json { render json: @menu_dium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menu_dia/1
  # PATCH/PUT /menu_dia/1.json
  def update
    respond_to do |format|
      if @menu_dium.update(menu_dium_params)
        format.html { redirect_to @menu_dium, notice: 'Menu dium was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu_dium }
      else
        format.html { render :edit }
        format.json { render json: @menu_dium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menu_dia/1
  # DELETE /menu_dia/1.json
  def destroy
    @menu_dium.destroy
    respond_to do |format|
      format.html { redirect_to menu_dia_url, notice: 'Menu dium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu_dium
      @menu_dium = MenuDium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_dium_params
      params.require(:menu_dium).permit(:Dia, :Estado)
    end
end
