class Pedido < ActiveRecord::Base
  validates :Cliente_id, presence: { message: " es requerido"}
  #validates :Empleado_id, presence: { message: " es requerido"}
  validates :EstadoPedido_id, presence: { message: " es requerido"}
  validates :FechaPedido, presence: { message: " es requerido"}
  validates :Mesa_id, presence: { message: " es requerido"}

  has_many :detalle_pedidos
  accepts_nested_attributes_for :detalle_pedidos
end
