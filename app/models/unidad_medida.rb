class UnidadMedida < ActiveRecord::Base
  validates :NombreCompleto, presence: { message: " es requerido"}
  validates :Unidad, presence: { message: " es requerido"}
end
