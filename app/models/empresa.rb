class Empresa < ActiveRecord::Base
  validates :Ciudad, presence: { message: " es requerido"}
  validates :Correo1, presence: { message: " es requerido"}
  validates :Direccion, presence: { message: " es requerido"}
  validates :NIT, presence: { message: " es requerido"}
  validates :NIT, length: { in: 8..20 , message: " es invalido"}
  validates :Nombre_empresa, presence: { message: " es requerido"}
  validates :Telefono, presence: { message: " es requerido"}
end
