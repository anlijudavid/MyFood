class DetalleValorNutricional < ActiveRecord::Base
  validates :Cantidad, presence: { message: " es requerido"}
  validates :NombreNutricional_id, presence: { message: " es requerido"}
  validates :Porcentaje, presence: { message: " es requerido"}
  validates :Porcentaje, numericality: { only_integer: true }
  validates :UnidadMedida_id, presence: { message: " es requerido"}
  validates :ValorNutricional_id, presence: { message: " es requerido"}
end
