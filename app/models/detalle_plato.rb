class DetallePlato < ActiveRecord::Base
  validates :Plato_id, presence: { message: " es requerido"}
  validates :CantidadIngrediente, presence: { message: " es requerido"}
  validates :Ingrediente_id, presence: { message: " es requerido"}
end
