class Ingrediente < ActiveRecord::Base
  validates :NombreIngrediente, presence: { message: " es requerido"}
  validates :TipoIngrediente_id, presence: { message: " es requerido"}
  validates :IVA, presence: { message: " es requerido"}
end
