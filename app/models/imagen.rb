class Imagen < ActiveRecord::Base
  validates :NombreImagen, presence: { message: " es requerido"}

  has_attached_file :image, :matches => [/png\Z/, /jpe?g\Z/, /gif\Z/], styles: { medium: '200x200>', thumb: '48x48>' }
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
