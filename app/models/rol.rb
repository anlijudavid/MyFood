class Rol < ActiveRecord::Base
  validates :NombreRol, presence: { message: " es requerido"}

  def ver_Rol (id_rol); true
    ro = find_by id, id_rol
    if ro.nil?
      format.html { redirect_to @permiso, notice: 'No hay permisos para realizar la busqueda' }
      format.json { render :show, status: :created, location: @permiso }
    end
    ro.NombreRol
  end
end
