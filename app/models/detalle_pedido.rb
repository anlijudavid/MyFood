class DetallePedido < ActiveRecord::Base
  validates :Plato_id, presence: { message: " es requerido"}
  validates :Cantidad, presence: { message: " es requerido"}
  validates :IVA, presence: { message: " es requerido"}
  validates :Pedido_id, presence: { message: " es requerido"}

  belongs_to :pedido
end
