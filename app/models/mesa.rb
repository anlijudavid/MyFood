class Mesa < ActiveRecord::Base
  validates :NumeroMesa, presence: { message: " es requerido"}
  validates :Maximo_personas, presence: { message: " es requerido"}
  validates :Maximo_personas, length: { in: 1..2 , message: "debe tener entre 1 y 2 caracteres"}
end
