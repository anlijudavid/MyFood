class Persona < ActiveRecord::Base
  validates :Nombre1, presence: { message: " es requerido"}
  validates :Apellido1, presence: { message: " es requerido"}
  validates :Departamento_id, presence: { message: " es requerido"}
  validates :Genero, presence: { message: " es requerido"}
end
