class Plato < ActiveRecord::Base
  belongs_to :TipoPlato
  belongs_to :Imagen
  validates :NombrePlato, presence: { message: " es requerido"}
  validates :PrecioUnitario, presence: { message: " es requerido"}
end
