Rails.application.routes.draw do


  devise_for :usuarios

  resources :home, only: [:index]

  authenticate :usuario do
    resources :detalle_sumistros, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :municipios, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :mesas, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :rols, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :departamentos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :permisos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :detalle_valor_nutricionals, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :nombre_nutricionals, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :detalle_valor_nutricionals, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :valor_nutricionals, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :valor_nutricionals, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :suministros, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :unidad_medidas, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :empresas, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :detalle_menu_dia, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :menu_dia, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :detalle_pedidos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :pedidos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :clientes, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :estado_pedidos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :empleados, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :personas, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :departamentos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :ciudads, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :detalle_platos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :ingredientes, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :detalle_platos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :tipo_ingredientes, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :platos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :platos, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :imagens, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :tipo_platos, only: [:index, :new, :create, :edit, :update, :destroy]
  end
  # resources :upload
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  get 'welcome/index'
  # get 'form/form'
  root 'static_pages#home'

 # get 'home/index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

end
